# Table of Contents
- [Formal Exam Presentation Setup](#formal-exam-presentation-setup)
  - [Background Information](#background-information)
  - [Local Exam Setup](#local-exam-setup)
  - [Test Bank Automation](#test-bank-automation)
    - [Establishing a token.pickle and client secret for GitLab Automation](#establishing-a-tokenpickle-and-client-secret-for-gitlab-automation)
    - [Configure the Pipeline With Your client secret](#configure-the-pipeline-with-your-client-secret)
    - [Use Your client secret in the Pipeline](#use-your-client-secret-in-the-pipeline)
- [Formal Exam Presentation Deployment](#formal-exam-presentation-deployment)
  - [GitLab Continuous Integration/Continuous Delivery (CI/CD) Pipeline](#gitlab-continuous-integrationcontinuous-delivery-cicd-pipeline)
    - [CSV and Master Question Google Sheet File Format](#csv-and-master-question-google-sheet-file-format)
    - [CSV Output Plain Text Example](#csv-output-plain-text-example)
    - [Google Sheets Output Example](#google-sheets-output-example)
  - [Exam Creation](#exam-creation)
  - [Exam Distribution](#exam-distribution)

# Formal Exam Presentation Setup

This document outlines the Formal Exam Presentation setup process for external organizations utilizing the 90th Cyberspace Operations Squadron Evaluations Element exam. These instructions are for the Google Suite formal exam component.

\[ [TOC](#table-of-contents) \]


## Background Information

| Term |Definition |
|------------|-------------|
| Exam | Set of questions designed to Examine an individual’s body of knowledge |
| Exam Material | Questions and answers used to create exams |
| Code Snippet | An image that contains code |
| Repo | Central location where data is stored on gitlab.com |
| Google Drive | A Google service to store and modify files |
| Google Sheets |A Google service to store and modify spreadsheets |
| Template | File used as a pattern for repeating processes |

\[ [TOC](#table-of-contents) \]


## Local Exam Setup
- Ensure there is Internet access and appropriate credentials to reach [Google Drive](https://accounts.google.com/).
  - Note:  A commercial Internet Service Provider (ISP) is necessary due to Non-classified Internet Protocol Router Network (NIPRNET) restrictions.
- If not already done, create a Google Drive account for your organization.  This account will be used to run the Knowledge Phase of your Exam.
- Log into your Google Drive account.
- Navigate to the Google Drive service by clicking the circle containing a dial pad (at top right, below bookmarks bar), then select “Drive” (see circled icon in Figure 1).


![Fig1](images/fig_1_exam-deployment.png)


The suggested folder structure is shown below. To help you get started, you can download our [test bank material](https://drive.google.com/drive/folders/1JhJEZW3xEr222MOyVXuz_u27nrkYMRRo?usp=sharing) and [knowledge test template](https://docs.google.com/spreadsheets/d/12tXNsGIv1ef8-0lv_6zjeEHkO1-uTp2Iylm4m15hEwE/copy?usp=sharing).

```
My Drive(root)
|
|— TestBank
|  |— code_snippets
|  |  |— work-role1
|  |  |— work-role2
|  |
|  |— work-role1_MQL
|  |— work-role2_MQL
|  |— Master_Question_List
|
|— KnowledgeTest_Template
```

Notes to the above directory tree image:
- work-role1 & work-role2 (when referring to both we use work-role#) refer to a specific work-role; for example, CCD for Basic Cyber Capability Developer or PO for Basic Product Owner.
- All images relating to a specific work-role are all kept within the respective `TestBank/code_snippets/work-role#/` folder.
- `work-role#_MQL` refers to a Google Sheets document containing all question data for the respective work-role.
- `Master_Question_List` refers to a Google Sheets document that is a combined Master Question File for all work-roles.
- `KnowledgeTest_Template` refers to a Google Sheets document containing the User Interface to generate knowledge exams.


Open and save the **KnowledgeTest_Template** Google Sheets template in the root location of your **My Drive.**

You will use that template, following the instructions in the yellow box, to create your knowledge exam.

To modify the script's behavior, open the script editor from within the `KnowledgeTest_Template` by selecting `Tools` -> `<> Script editor`.

\[ [TOC](#table-of-contents) \]


## Test Bank Automation
These instructions help you setup your knowledge test bank repository so your question content will be available to your Google Drive.

\[ [TOC](#table-of-contents) \]


### Establishing a token.pickle and client secret for GitLab Automation
In order to have your Test Bank files pushed to your Google Drive automatically, it is necessary for you to create a token.pickle file. 
- If not already there, install Python 3 on your system 
- Follow the steps in [Python Quickstart](https://developers.google.com/drive/api/v3/quickstart/python) to generate your token.pickle and client secret using the following modification.
  - In step 3, change the value of `SCOPES` to ["https://www.googleapis.com/auth/drive", "https://www.googleapis.com/auth/drive.file"]

Now that you have a token.pickle for your Google Drive Account, you should be aware that the client secret is in the token.pickle file and must be removed before saving it to GitLab. The script that pushes files to your google drive will add it back by specifying it as a command line input variable.
- Open token.pickle in a text editor and search for `_client_secret`
- Remove the characters that represent your `client secret` that was provided to you during the `Python Quickstart` setup
- Save the file when you are finished and place it in `scripts/formal_exam_pres/google_suite/support` of your knowledge test bank.

\[ [TOC](#table-of-contents) \]


### Configure the Pipeline With Your client secret
Your client secret will need to be added to your Test Bank's CI/CD variable settings. Note, someone with permissions to modify repository settings will have to complete this.
- After logging into your GitLab Knowledge Test Bank repository, select `Settings` -> `CI / CD`
- Expand the `Variables` section and then click `Add Variable`
- Enter the name of your variable in the `Key` box
- Enter your client secret's value in the `Value` box
- Ensure the box labeled `Mask variable` is selected
- Click the `Add variable` button and you will see your new variable appear

\[ [TOC](#table-of-contents) \]


### Use Your client secret in the Pipeline
To use your pipeline variable, edit your pipeline file and place the name of the variable where it needs to be used.

Using an example from a file called `google_suite_ci.yml` and a job titled `google_suite` that pushes to Google Drive:
- Locate the yml file with Google Suite jobs; the default location is `scripts/formal_exam_pres/google_suite/google_suite_ci.yml`
- Locate the job that pushes to Google Drive; the default job is `google_suite`
- Add your token variable name to the script that pushes files; in this example, we have called the variable `GDRVKEY`.
```yml
google_suite:
  stage: deploy
  script: 
    - python3 scripts/formal_exam_pres/google_suite/google_drive_uploader.py GDRVKEY
```

\[ [TOC](#table-of-contents) \]

# Formal Exam Presentation Deployment
This explains the process of deploying an exam.

\[ [TOC](#table-of-contents) \]

## GitLab Continuous Integration/Continuous Delivery (CI/CD) Pipeline
The GitLab CI/CD pipeline collects all the questions in your test bank and puts the data into a format that can be easily utilized by the Formal Exam component. The pipeline creates a comma separated values (CSV) file with all test bank data, along with any applicable images, and pushes this data to your Google Drive using a token.pickle and client secret that you established. The CSV file is converted to a Google Sheets document prior to pushing the data to your Google Drive; this is necessary for your data to be read into the Google Form that will become your exam.

\[ [TOC](#table-of-contents) \]

### CSV and Master Question Google Sheet File Format
The script that generates the CSV and images is called `gen_knowl_exam_csv.py` and is located in `scripts/formal_exam_pres/google_suite/` of your knowledge test bank. This script locates all your knowledge test bank JSON files with question content and generates the correct headers and data for each column. The information below is simply meant to explain the resulting CSV output. Since the CSV file is converted to a Google Sheet, both versions require the same format.

During the `Knowledge Phase Exam Setup`, you copied a Google Sheet file called `KnowledgeTest_Template` to your Google Drive. The `KnowledgeTest_Template` contains a script called `FormCreator.gs` that parses your Master Question File and generates a Google Form that is an exam. The `FormCreator` script uses a header row to determine the different parts of each question. The order of columns within the header row is insignificant except for those identifying the mulitple choice options; in which case you will need to ensure they are next to each other from `A - Z` order. Note, a maximum of 9 multiple choice options is supported. The information below explains each entry that must be present in the header row; note, the header row must be first in the file.

| Header Column Name | Explanation |
| :--- | :--- |
| **Question Type:** | This tells the script to create specific types of questions; however, only `CHOICE` is currently supported. |
| **Question Name:** | This is the name of the question as defined in your Knowledge Test Bank. |
| **Question:** | This is the question itself. |
| **A** - **I** | This is a single alpha character identifying one of the multiple-choice options. <br>  - Note, these must appear in the header from A - Z order with nothing else mingled between. <br>  - If your questions have a max of 5 options then your header will contain `A, B, C, D, E` |
| **Answer:** | This stores a single alpha character representing the correct answer. |
| **Explanation:** | This is a list of one or more explanations for why an option is correct or incorrect. |
| **Points:** | This determines the number of points awarded when a question is answered correctly. |
| **Subject:** | This is the name of the subject within a particular work-role (i.e. Agile, Python, etc.). |
| **Training Ref:** | This points a person to a training reference document. |
| **Image Name:** | For questions with snippets, this is the name of the snippet generated from the pipeline for the question. Questions without snippets will have nothing in this column. |
| **Work Role:** | This is the name of the work-role in which this question applies. |
| **UID:** | This is a unique identifier for your question (which should be the same as the _id in your question's json file). |

In addition to the mandatory header row titles above, the `gen_knowl_exam_csv.py` script generating the CSV also adds the following additional information that may be used for reference information and possible future features.

| Header Column Name | Explanation |
| :--- | :--- |
| **Snippet:** | This is the actual text for this question; if the question does not have a snippet then this is blank. |
| **Group Name:** | This identifies the name of the question group. If a question is not part of a group, then this is blank. |
| **Group Question UIDs:** | This is a list of question IDs that belong to the question group. If a question is not part of a group, then this is blank. |
| **Proficiency Code:** | This identifies the proficiency the question was written to and determines the level of knowledge a person has. |
| **Est. Time to Complete:** | This is an amount in seconds that estimates the time it takes for someone to complete this question. |
| **KSATs:** | This is a list of the applicable Knowledge, Skills, Abilities, and Tasks (KSATs) for a question. |
| **Version:** | This identifies the version of this question. |
| **Date Created:** | This is the date the question was created. |
| **Date Revised:** | This is the date the question was last revised. |
| **Attempts:** | This is the number of times the question was attempted. |
| **Passes:** | This is the number of times the question was answered correctly. |
| **Failures:** | This is the number of times the question was answered incorrectly. |
| **Reviewer Section:** | This is a space for question reviewers to provide comments during the approval process for a question. |
| **Training Comments/Suggestions:** | This provides additional information about this question. |

\[ [TOC](#table-of-contents) \]

### CSV Output Plain Text Example
When the `gen_knowl_exam_csv.py` is finished, the CSV output will look something like the following (as seen in a text editor):

```text
Question Type:,Question Name:,Question:,A,B,C,D,E,Answer:,Points:,Subject:,Training Ref:,Image Name:,Snippet:,Explanation:,Group Name:,Group Question UIDs:,Proficiency Code:,Est. Time to Complete:,KSATs:,Work Role:,UID:,Version:,Date Created:,Date Revised:,Attempts:,Passes:,Failures:,Reviewer Section:,Training Comments/Suggestions:
CHOICE,"agile1.question.json","True or False: ... ?","option A","option B",,,,A,1,"Agile",,,,"An explanation detailed enough to explain all options",,,B,1,"K0060
K0700",['PO'],PO_AGILE_0001,"1","2020-06-16","2020-03-13 19:43:08",0,0,0,,
CHOICE,"doubly-linked-list.question.json","The above C struct definition ... ?","option A","option B","option C","option D","option E",A,1,"Data Structures",,BD_DSTRUC_0003.png,"1  |  struct Node {
2  |    int data;
3  |    struct Node *next;
4  |    struct Node *prev;
5  |  };
6 | ","A is Correct because ...
B is incorrect because ...
C is incorrect because ...
D is incorrect because ...
E is incorrect because ...",,,B,1,"K0057
K0700",['CCD'],BD_DSTRUC_0003,"1","2020-06-16","2020-03-13 19:43:08",0,0,0,,
CHOICE,"python-group_1/data-types_4.question.json","What is ... ?","option A","Option B","option C","option D",,B,1,"Python",,BD_PY_0017.png,"1  | def getAverage(GoT):
2  |    tot = 0
3  |    count = 0
4  |    for rank in GoT.values():
5  |       tot += rank
6  |       count += 1
7  | 
8  |    return int(tot/count)
9  | ","An explanation detailed enough to explain all options",python-group_1,"BD_PY_0016
BD_PY_0019
BD_PY_0017
BD_PY_0023
BD_PY_0020
BD_PY_0022
BD_PY_0021
BD_PY_0018
BD_PY_0015",B,2.25,"K0008
K0690",['CCD'],BD_PY_0017,"1","2020-06-16","2020-03-13 19:43:08",2,2,0,,
...
```

\[ [TOC](#table-of-contents) \]

### Google Sheets Output Example
When seen in an application such as Google Sheets or MS Excel:

| Question Type:    | Question Name:    | Question: | A | B | C | D | E | Answer:   | Points:   | Subject:  | Training Ref: | Image Name:   | Snippet:  | Explanation:  | Group Name:   | Group Question UIDs:  | Proficiency Code: | Est. Time to Complete:    | KSATs:    | Work Role:    | UID:  | Version:  | Date Created: | Date Revised: | Attempts: | Passes:   | Failures: | Reviewer Section: | Training Comments/Suggestions: |
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
| CHOICE | "agile1.question.json" | "True or False: ... ?" | option A | option B |   |   | | A | 1 | "Agile" | | | | "An explanation detailed enough to explain all options" | | | B | 1 | K0060 <br> K0700 | ['PO'] | PO_AGILE_0001 | "1" | "2020-06-16" | "2020-03-13 19:43:08" | 0 | 0 | 0 | |
| CHOICE| "doubly-linked-list.question.json"| "The above C struct definition ... ?"| option A | option B | option C | option D | option E | A | 1 | "Data Structures"| | BD_DSTRUC_0003.png| "1  \|  struct Node {<br>2  \|    int data;<br>3  \|    struct Node *next;<br>4  \|    struct Node *prev;<br>5  \|  };<br>6 \| "| "A is Correct because ...<br>B is Incorrect because ...<br>C is Inorrect because ...<br>D is Inorrect because ...<br>E is Incorrect because ..."| | | B| 1| "K0057 <br>K0700"| ['CCD']| BD_DSTRUC_0003| "1"| "2020-06-16"| "2020-03-13 19:43:08"| 0| 0| 0| | |
|CHOICE|"python-group_1/data-types_4.question.json"|"What is ... ?"|"option A"|"Option B"|"option C"|"option D"||B|1|"Python"||BD_PY_0017.png|"1  \| def getAverage(GoT):<br>2  \|    tot = 0<br>3  \|    count = 0<br>4  \|    for rank in GoT.values():<br>5  \|       tot += rank<br>6  \|       count += 1<br>7  \| <br>8  \|    return int(tot/count)<br>9  \| "|"An explanation detailed enough to explain all options"|python-group_1|"BD_PY_0016<br>BD_PY_0019<br>BD_PY_0017<br>BD_PY_0023<br>BD_PY_0020<br>BD_PY_0022<br>BD_PY_0021<br>BD_PY_0018<br>BD_PY_0015"|B|2.25|"K0008 <br>K0690"|['CCD']|BD_PY_0017|"1"|"2020-06-16"|"2020-03-13 19:43:08"|2|2|0|||
| ... |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |

\[ [TOC](#table-of-contents) \]

## Exam Creation
This explains how to create a new exam using the Google Sheets user interface in your Google Drive.
- Ensure there is Internet access and appropriate credentials to reach Google Drive at https://accounts.google.com.
- Login to your Google Suite account using the required credentials.
- Navigate to the Google Drive service by clicking the circle containing a dial pad (at top right, below bookmarks bar), then select `Drive` (see circled icon in Figure 1).
<br><br>
![Fig1](images/fig_1_exam-deployment.png)
<br><br>
- Click the file `KnowledgeTest_Template` to open the exam template.  Complete the template following the instructions provided in the spreadsheet (see sample in Figure 2).
<br><br>
![Fig2](images/fig_2_exam-deployment.png)
<br><br>
- When complete, click `Create` to generate a Google form.  A pop-up labeled `Running script` will appear while the form is processing; this will take approximately 2-3 minutes.
- Once the pop-up `Finished script` appears, the newly created form is available in the account's Google Drive, titled by the name provided in the cell to the right of the `Exam Title` cell.  The Google sheet used to collect form submissions will appear in Google Drive titled by the `Exam Title` + `_responses`.

Open up the newly created exam and modify the following settings by clicking on the icon in the upper right corner that looks like a gear. These modifications will ensure a copy of the results is sent to examinees and allows examinees to see the correct vs chosen options, feedback and their score.
- In the General tab, by default the `Collect email addresses->Response receipts->If respondent requests it` option is selected. Change `If respondent requests it` to `Always`; your settings window should look like Figure 3 below.
<br><br>
![Fig3](images/fig_3_exam-deployment.png)
<br><br>
- In the `Quizzes` tab, by default `Make this a quiz` is enabled, `Release grade->Later, after manual review` is selected and no options are selected under `Respondent can see:`. 
  - Change `Later, after manual review` to `Immediately after each submission`
  - Select all options under `Respondent can see:`; your settings window should look like Figure 4.
  - When you're finished, click the `Save` button.
<br><br>
![Fig4](images/fig_4_exam-deployment.png)
<br><br>

\[ [TOC](#table-of-contents) \]

## Exam Distribution
This explains how to get a link to the form that examinees can access to take their exam.
- Locate and open the newly created knowledge-based form in Google Drive.
- Once the form opens, click the `SEND` button in the upper right-hand corner.
- Once the pop-up window appears, click the link icon (see red outline in Figure 5) and copy the URL below the word `Link`. 
- Distribute the link to examinees so they can take their exam.
<br><br>
![Fig5](images/fig_5_exam-deployment.png)
<br><br>

\[ [TOC](#table-of-contents) \]
