#!/usr/bin/python3

import os
import sys
import json
import argparse
import pymongo
from colorama import Fore
from support import markdown_gen
# Move up the necessary amount of directories so the scripts path is inserted into sys.path
# In this case, it takes 4 directories to get to the scripts folder
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))))
from shared.lib.errors import ErrorCode
from shared.lib.file_list_generator import dir_walk, changed_paths
from shared.lib import util
from shared.lib.default_strings import opt_param_changed_desc, param_question_dir_desc, error_str, warning_str, \
    keyword_default, keyword_desc, readme, question_type_key, rellink_id_key
from shared.lib.mttl_db_helpers import HOST_NAME, PORT_NUM, get_question_workroles

db_name_default = "mttl"
db_name_desc = "This is the name of the data base and is used for DB connection purposes. " \
               f"The default is {db_name_default}"
description = "This module allows generation of each question's README.md file using its json file."
mongo_host_desc = f"This is the host name where the Mongo Database is running. The default is '{HOST_NAME}'"
mongo_port_desc = f"This is the port where the Mongo Database is running. The default is '{PORT_NUM}'"
out_dir_default = "."
out_dir_desc = "The directory to store the resulting Question README Generation report, relative to root_dir. " \
               "The default is '{}'.".format(out_dir_default)
report_file_default = "QuestionReadmeGenReport.json"
report_file_desc = "The name of the report file; the default is '{}'.".format(report_file_default)
root_dir_desc = "This is the location of the knowledge material and may be a relative path. This is necessary to " \
                "ensure the links are properly setup."

msg_er_color = "{0} {1}:".format(Fore.RED + os.path.basename(__file__), error_str + Fore.RESET)
msg_er = "{0} {1}:".format(os.path.basename(__file__), error_str)
msg_wn_color = "{0} {1}:".format(Fore.YELLOW + os.path.basename(__file__), warning_str + Fore.RESET)
msg_wn = "{0} {1}:".format(os.path.basename(__file__), warning_str)
msg_gd_color = "{0}:".format(Fore.GREEN + os.path.basename(__file__) + Fore.RESET)
msg_gd = "{0}:".format(os.path.basename(__file__))


def gen_readme_md(args: argparse.Namespace):
    '''
    Purpose: Allows generation of each question's README.md file from its json file.
    :return: status - a dictionary with keys representing error codes according to class Error or the exception, when
             present.
    '''
    status = {}  # Stores any errors that may occur
    out_dir = args.out_dir
    if args.out_dir != ".":
        # Eliminates the redundant ././ when combining root_dir and out_dir
        out_dir = os.path.join(args.root_dir, args.out_dir)
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    if args.question_dirs is None:
        args.question_dirs = ["."]
    # Get Mongo DB connection
    host = os.getenv('MONGO_HOST', args.Host)
    port = int(os.getenv('MONGO_PORT', args.Port))
    client = pymongo.MongoClient(host, port)
    db = client[args.db_name]
    if args.changed:
        # In case only changed files are requested
        paths = changed_paths(repo_base_dir=args.root_dir, keyword=args.filter)
    else:
        # In case all files are requested
        paths = dir_walk(root_dir=args.root_dir, json_dirs=args.question_dirs, keyword=args.filter)
    if len(paths) == 0:
        # In case the list is empty
        msg = "{0} {1}: Received empty file list."
        print(msg.format(Fore.YELLOW + os.path.basename(__file__), warning_str + Fore.RESET))
        status[1] = msg.format(os.path.basename(__file__), warning_str)
        return status
    for item in paths:
        # Process each question
        # Stores any previous README.md data
        prev_readme = ""
        # Retrieve the relative path to the JSON file's directory; convert any possible backslashes to forward
        rel_dir = os.path.dirname(os.path.relpath(item, os.getcwd())).replace("\\", "/")
        if not rel_dir.endswith("/"):
            # In case the relative directory path doesn't have a trailing forward slash
            rel_dir += "/"
        # Retrieve the relative path to knowledge/README.md from the JSON file's directory
        back_path = os.path.relpath(args.root_dir, os.path.dirname(item)).replace("\\", "/")
        if not back_path.endswith("/"):
            # In case the relative directory path doesn't have a trailing forward slash
            back_path += "/"
        try:
            with open(item, "r") as q_fp:
                data = json.load(q_fp)  # Get question as a dict object
            if question_type_key not in data or question_type_key in data and data[question_type_key] != "knowledge":
                # Since this is only meant to work for knowledge questions, skip this file
                if ErrorCode.SUCCESS.value not in status:
                    status[ErrorCode.SUCCESS.value] = []
                status[ErrorCode.SUCCESS.value].append(
                    "Skipping '{}' because it's not a knowledge question rel-link.".format(item))
                continue
            rellink_id = data[rellink_id_key]
            if rellink_id is None or rellink_id == "":
                print(f"{Fore.YELLOW}The MTTL MongoDB rel-link ObjectID for '{Fore.CYAN + item + Fore.YELLOW}'"
                      f" cannot be blank.{Fore.RESET}")
                continue
        except json.JSONDecodeError as err:
            msg = "{0} Unable to open {1}\n{2}"
            if ErrorCode.JSON_DECODE_ERROR.value not in status:
                status[ErrorCode.JSON_DECODE_ERROR.value] = []
            status[ErrorCode.JSON_DECODE_ERROR.value].append(msg.format(msg_er, item, err))
            continue
        else:
            try:
                if os.path.exists(os.path.join(rel_dir, readme)):
                    # In case there's already an existing README.md file, store contents to see if anything changed
                    with open(os.path.join(rel_dir, readme), "r") as md_file:
                        prev_readme = md_file.readlines()
            except Exception as e:
                msg = "{0} Unable to open {1} for reading.\n{2}"
                if ErrorCode.INVALID_INPUT.value not in status:
                    status[ErrorCode.INVALID_INPUT.value] = []
                status[ErrorCode.INVALID_INPUT.value].append(
                    msg.format(msg_er, os.path.join(args.root_dir, readme), e))
                continue
            else:
                # In case the file was read successfully or doesn't exist, convert prev_readme into a string
                prev_readme = "".join(prev_readme)
                gen_snippet = True
                back_links = []
                # In case the question is decoded properly
                workroles = get_question_workroles(data[rellink_id_key], db)
                if len(workroles) < 0:
                    msg = "{0} Unable to get mapped work-roles."
                    if ErrorCode.INVALID_JSON_FORMAT.value not in status:
                        status[ErrorCode.INVALID_JSON_FORMAT.valu] = []
                    status[ErrorCode.INVALID_JSON_FORMAT.valu].append(msg.format(msg_er))
                    continue
                for workrole in workroles:
                    # Process links for each workrole
                    back_links.append(back_path + readme)
            md_content = markdown_gen.markdown_generator(item, back_links, db, gen_snippet)
            # Determine if the markdown data was generated successfully
            if type(md_content) is int:
                # Since the function only returns str or int
                msg = "{0} Markdown Generation Error"
                if md_content not in status:
                    status[md_content] = []
                status[md_content].append(msg.format(msg_er))
                continue
            if prev_readme != md_content:
                # Process and write the README.md file to the question's directory
                util_status = util.write_str_totext(md_content, os.path.join(rel_dir, readme))
                for code, rtn_status in util_status.items():
                    if code not in status:
                        status[code] = []
                    status[code].append(rtn_status)
            else:
                msg = "{0} No updates necessary for '{1}'."
                if ErrorCode.SUCCESS.value not in status:
                    status[ErrorCode.SUCCESS.value] = []
                status[ErrorCode.SUCCESS.value].append(msg.format(msg_gd, os.path.join(rel_dir, readme)))

    try:
        json.dump(status, open(os.path.join(out_dir, args.report_file), "w"), indent=4)
    except (IOError, json.JSONDecodeError, ValueError, TypeError, OverflowError) as err:
        msg = "{0} Could not create the Question README Generator report file '{1}'; {2}"
        print(msg.format(msg_er_color, Fore.CYAN + os.path.join(out_dir, args.report_file) + Fore.RESET, err))
        if ErrorCode.JSON_DECODE_ERROR.value not in status:
            status[ErrorCode.JSON_DECODE_ERROR.value] = []
        status[ErrorCode.JSON_DECODE_ERROR.value].append(
            msg.format(msg_er, os.path.join(out_dir, args.report_file), err))
    else:
        if len(status) == 1 and ErrorCode.SUCCESS.value in status:
            # In case the process was successful
            print("{0} Question README.md's generated successfully; log written to '{1}'.".format(
                msg_gd_color,
                Fore.CYAN + os.path.join(out_dir, args.report_file) + Fore.RESET
            ))
        else:
            print("{0} refer to '{1}' for details.".format(
                msg_er_color,
                Fore.CYAN + os.path.join(out_dir, args.report_file) + Fore.RESET
            ))
    finally:
        return status


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument("root_dir", type=str, help=root_dir_desc)
    parser.add_argument("-d", "--db_name", type=str, default=db_name_default, help=db_name_desc)
    parser.add_argument("-c", "--changed", action="store_true", help=opt_param_changed_desc)
    parser.add_argument("-f", "--filter", type=str, default=keyword_default, help=keyword_desc)
    parser.add_argument("-H", "--Host", type=str, default=HOST_NAME, help=mongo_host_desc)
    parser.add_argument("-o", "--out_dir", type=str, default=out_dir_default, help=out_dir_desc)
    parser.add_argument("-P", "--Port", type=str, default=PORT_NUM, help=mongo_port_desc)
    parser.add_argument("-q", "--question_dirs", nargs='+', type=str, default=None, help=param_question_dir_desc)
    parser.add_argument("-r", "--report_file", type=str, default=report_file_default, help=report_file_desc)
    params = parser.parse_args()
    status = gen_readme_md(params)
    if ErrorCode.SUCCESS.value in status and len(status) > 1:
        # In case status has errors, prevent returning zero
        del status[ErrorCode.SUCCESS.value]
    sys.exit(list(status.keys())[0])
