#!/usr/bin/python3

import os
import sys
import json
import argparse
import pymongo
from colorama import Fore
from support import markdown_gen
# Move up the necessary amount of directories so the scripts path is inserted into sys.path
# In this case, it takes 4 directories to get to the scripts folder
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))))
from shared.lib import util
from shared.lib.errors import ErrorCode
from shared.lib.file_list_generator import dir_walk, changed_paths
from shared.lib.default_strings import opt_param_changed_desc, group_key, group_name_key, group_questions_key, \
    topic_key, error_str, warning_str, keyword_desc, keyword_default, readme, question_type_key
from shared.lib.mttl_db_helpers import HOST_NAME, PORT_NUM

subject_header = "## ***{subject}***\n\n"
group_begin_msg = "\n---\n\n### Begin {group} Questions\n\n"
group_begin_msg += "Please refer to the code snippet below for the following {number} question(s).\n\n"
group_end_msg = "\n### End {group} Questions\n\n---\n\n"

db_name_default = "mttl"
db_name_desc = "This is the name of the data base and is used for DB connection purposes. " \
               f"The default is {db_name_default}"
description = "Allows updating the master question list in the Knowledge repo's README.md file from the auto.py script."
mongo_host_desc = f"This is the host name where the Mongo Database is running. The default is '{HOST_NAME}'"
mongo_port_desc = f"This is the port where the Mongo Database is running. The default is '{PORT_NUM}'"
out_dir_default = "."
out_dir_desc = "The directory to store the resulting Master Question List Updater report, relative to root_dir. " \
               "The default is '{}'.".format(out_dir_default)
report_file_default = "MQLUpdaterReport.json"
report_file_desc = "The name of the report file; the default is '{}'.".format(report_file_default)
root_dir_desc = "This is the location of the knowledge material and may be a relative path. This is necessary to " \
                "ensure the README.md file is updated in the correct location and the links are properly setup."

msg_er_color = "{0} {1}:".format(Fore.RED + os.path.basename(__file__), error_str + Fore.RESET)
msg_er = "{0} {1}:".format(os.path.basename(__file__), error_str)
msg_wn_color = "{0} {1}:".format(Fore.YELLOW + os.path.basename(__file__), warning_str + Fore.RESET)
msg_wn = "{0} {1}:".format(os.path.basename(__file__), warning_str)
msg_gd_color = "{0}:".format(Fore.GREEN + os.path.basename(__file__) + Fore.RESET)
msg_gd = "{0}:".format(os.path.basename(__file__))


def update_mqls(args: argparse.Namespace) -> dict:
    '''
    Purpose: Allows updating each <work role> master question list in the Knowledge repo.
    :return: status - a dictionary with keys representing error codes according to class Error or the exception, when
             present.
    '''
    # Stores any errors that may occur
    status = {}
    rel_link_dirs = ["."]
    prev_sub = ""
    prev_grp_status = False
    prev_grp_header = ""
    out_dir = args.out_dir
    if args.out_dir != ".":
        # Eliminates the redundant ././ when combining root_dir and out_dir
        out_dir = os.path.join(args.root_dir, args.out_dir)
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    # Get Mongo DB connection
    host = os.getenv('MONGO_HOST', args.Host)
    port = int(os.getenv('MONGO_PORT', args.Port))
    client = pymongo.MongoClient(host, port)
    db = client[args.db_name]
    if args.changed:
        # In case only changed files are requested
        paths = changed_paths(repo_base_dir=args.root_dir, keyword=args.filter)
    else:
        # In case all files are requested
        paths = dir_walk(root_dir=args.root_dir, json_dirs=rel_link_dirs, keyword=args.filter)
    if len(paths) == 0:
        # In case the list is empty
        msg = "{0} Received empty file list."
        print(msg.format(msg_er_color))
        status[ErrorCode.ErrorCode.EMPTY_LIST.value] = [msg.format(msg_er)]
        return status

    old_mql = ""  # Stores the contents of the <work role>.md file before changes
    # Stores the current contents of the <work role>.md file as questions are added
    new_mql = "# Master Question List\n\n"
    if os.path.exists(os.path.join(args.root_dir, readme)):
        with open(os.path.join(args.root_dir, readme), "r") as mql_file:
            old_mql = "".join(mql_file.readlines())
    # Start with a string sort
    paths.sort()
    # Now fix incremental numbering at the end of folder names to be in numerical order
    paths = util.custom_sort(paths)
    for item in paths:
        # Process each question
        if not os.path.exists(item):
            # In case the path is bad
            msg = "\t{0} Couldn't find {1}."
            if ErrorCode.INVALID_INPUT.value not in status:
                status[ErrorCode.INVALID_INPUT.value] = []
            status[ErrorCode.INVALID_INPUT.value].append(msg.format(msg_er, item))
            continue
        try:
            # Process and write the Master Question List for each work role
            with open(item, "r") as q_fp:
                json_data = json.load(q_fp)
            if question_type_key not in json_data or (question_type_key in json_data and
                                                      json_data[question_type_key] != "knowledge"):
                # Since this is only meant to process knowledge questions, skip this file
                continue
            # Get the answer's relative path and convert to universal directory separator, which is forward slash
            # Ensure relative path is in relation to the root_dir path
            ans_dir = os.path.relpath(os.path.dirname(item), args.root_dir).replace("\\", "/")
            if not ans_dir.endswith("/"):
                ans_dir += "/"  # Ensure the path ends with a trailing forward slash
            if group_key in json_data:
                # In case this question is part of a group
                group_status = True  # This is a group question
                group_header = json_data[group_key][group_name_key]
            else:
                # In case this is not a group question
                group_status = False
                group_header = ""
            if group_status and prev_grp_status and group_header == prev_grp_header:
                # In case the group's snippet has already been written to file
                gen_snippet = False
            else:
                # In case this is the first group question or it's not a group question
                gen_snippet = True  # Yes, generate the code snippet
                if prev_grp_status and group_header != prev_grp_header:
                    # In case the group footer should be written
                    new_mql += group_end_msg.format(group=prev_grp_header.upper())
                if group_status:
                    # In case the group header should be written
                    new_mql += group_begin_msg.format(
                        group=group_header.upper(),
                        number=len(json_data[group_key][group_questions_key])
                    )
            # Get the contents to write to the file
            md_content = markdown_gen.markdown_generator(item, [ans_dir], db, gen_snippet, False)
            # Determine if the markdown data was generated successfully
            if type(md_content) is int:
                # In case an error was returned
                msg = "{0} Markdown Generation Error"
                if md_content not in status:
                    status[md_content] = []
                status[md_content].append(msg.format(msg_er))
                continue
            if prev_sub != json_data[topic_key]:
                # In case this is the beginning of a new subject
                new_mql += subject_header.format(subject=json_data[topic_key])
            prev_sub = json_data[topic_key]  # Update previous subject
            prev_grp_status = group_status  # Update previous group status
            prev_grp_header = group_header  # Update previous group header
            new_mql += "{0}\n".format(md_content)
        except json.JSONDecodeError as err:
            msg = "{0} loading {1}:\n{2}"
            if ErrorCode.JSON_DECODE_ERROR.value not in status:
                status[ErrorCode.JSON_DECODE_ERROR.value] = []
            status[ErrorCode.JSON_DECODE_ERROR.value].append(msg.format(msg_er, item, err))
            continue
        except Exception as err:
            msg = "{0} while processing {1}:\n{2}"
            if ErrorCode.UNABLE_TO_GEN_MARKDOWN.value not in status:
                status[ErrorCode.UNABLE_TO_GEN_MARKDOWN.value] = []
            status[ErrorCode.UNABLE_TO_GEN_MARKDOWN.value].append(msg.format(msg_er, item, err))
            continue
    if old_mql == new_mql:
        # In case there are no changes from the old to the new content.
        msg = "{0} {1} already contains the latest info."
        if ErrorCode.SUCCESS.value not in status:
            status[ErrorCode.SUCCESS.value] = []
        status[ErrorCode.SUCCESS.value].append(msg.format(msg_gd, readme))
    else:
        # In case changes occurred.
        util_status = util.write_str_totext(new_mql, os.path.join(args.root_dir, readme))
        for code, rtn_status in util_status.items():
            if code not in status:
                status[code] = []
            status[code].append(rtn_status)
    try:
        json.dump(status, open(os.path.join(out_dir, args.report_file), "w"), indent=4)
    except (IOError, json.JSONDecodeError, ValueError, TypeError, OverflowError) as err:
        msg = "{0} Could not create the MQL Updater report file '{1}'; {2}"
        print(msg.format(msg_er_color, Fore.CYAN + os.path.join(out_dir, args.report_file) + Fore.RESET, err))
        if ErrorCode.JSON_DECODE_ERROR.value not in status:
            status[ErrorCode.JSON_DECODE_ERROR.value] = []
        status[ErrorCode.JSON_DECODE_ERROR.value].append(
            msg.format(msg_er, os.path.join(out_dir, args.report_file), err))
    else:
        if len(status) == 1 and ErrorCode.SUCCESS.value in status:
            # In case the process was successful
            print("{0} MQL README.md updated successfully; log written to '{1}'.".format(
                msg_gd_color,
                Fore.CYAN + os.path.join(out_dir, args.report_file) + Fore.RESET
            ))
        else:
            print("{0} refer to '{1}' for details.".format(
                msg_er_color,
                Fore.CYAN + os.path.join(out_dir, args.report_file) + Fore.RESET
            ))
    finally:
        return status


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument("root_dir", type=str, help=root_dir_desc)
    parser.add_argument("-d", "--db_name", type=str, default=db_name_default, help=db_name_desc)
    parser.add_argument("-c", "--changed", action="store_true", help=opt_param_changed_desc)
    parser.add_argument("-f", "--filter", type=str, default=keyword_default, help=keyword_desc)
    parser.add_argument("-H", "--Host", type=str, default=HOST_NAME, help=mongo_host_desc)
    parser.add_argument("-o", "--out_dir", type=str, default=out_dir_default, help=out_dir_desc)
    parser.add_argument("-P", "--Port", type=str, default=PORT_NUM, help=mongo_port_desc)
    parser.add_argument("-r", "--report_file", type=str, default=report_file_default, help=report_file_desc)
    params = parser.parse_args()
    status = update_mqls(params)
    if ErrorCode.SUCCESS.value in status and len(status) > 1:
        # In case status has errors, prevent returning zero
        del status[ErrorCode.SUCCESS.value]
    sys.exit(list(status.keys())[0])
