#!/usr/bin/python3

import os
import subprocess

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))

db_name = 'knowledge_test_bank'
test_banks_path = 'test-banks'

def import_file_mongo(tb_name:str, file_path:str):
    cmd = f'mongoimport --host {HOST} --db {db_name} --collection {tb_name} --file {file_path}'
    subprocess.run(cmd, shell=True)

def drop_mongo_db():
    cmd = f'mongo --host {HOST} --eval \'db.dropDatabase()\' {db_name}'
    subprocess.run(cmd, shell=True)

def main():
    drop_mongo_db()
    test_bank_dirs = [f.path for f in os.scandir(test_banks_path) if f.is_dir()]
    for test_bank_dir in test_bank_dirs:
        test_bank_name = os.path.basename(test_bank_dir)
        for root, dirs, files in os.walk(test_bank_dir):
            for name in files:
                if 'question.json' in name:
                    # continue
                    import_file_mongo(test_bank_name, os.path.join(root, name))

if __name__ == "__main__":
    main()

