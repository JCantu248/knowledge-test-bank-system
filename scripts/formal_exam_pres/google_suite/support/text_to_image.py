import os
import subprocess
import sys
import json
from colorama import Fore
from PIL import Image, ImageDraw, ImageFont
# Move up the necessary amount of directories so the scripts path is inserted into sys.path
# In this case, it takes 5 directories to get to the scripts folder
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))))))
from shared.lib.default_strings import id_key, error_str, warning_str, snippet_key, topic_key

font_file = "font_files/monaco.ttf"
default_font_file = "monaco.ttf"
image_type = "png"
base_image_path = "code_snippets/"


def get_out_dir(topic: str, out_dir: str = base_image_path) -> str:
    '''
    Purpose: This function determines the output directory according to the question's topic
    :param topic: the topic of the question
    :param out_dir: The base output directory for the images
    :return: rel_path = A string representing the path for the generated image file to be copied
    '''
    # Make sure the base image path ends in a slash and convert any MS directory separators to forward slash notation
    out_dir = out_dir.replace("\\", "/")
    if not out_dir.endswith("/"):
        out_dir += "/"

    return os.path.join(out_dir, topic.replace(" ", "_"))


def resolve_fontfile_path(start_dir: str = "."):
    '''
    Purpose: The resolve_fontfile_path function tries to find the font file's relative path to the current working
    directory.
    :param start_dir: The starting directory for file searches
    :return: Upon success, the relative path to the font file; or if not found, an empty string
    '''
    for (root, dirs, files) in os.walk(start_dir):
        # Search the current folder & sub-folders for the font file's path
        for file in files:
            if default_font_file in file:
                return os.path.join(root, file).replace("\\", "/")
    return ""


def mermaid_to_image(file_name: str, snippet: str, topic: str, qid: str, out_dir: str = base_image_path):
    '''
    Purpose: Convert a string representation of a mermaid snippet into an image for the knowledge exam.
    :param file_name: A string representing the name of the question's file.
    :param snippet: A string representing the mermaid snippet
    :param topic: A string representing the question's topic
    :param qid: A string representing the question's ID
    :param out_dir: A string representing the output directory for the image
    :return: img_fname - A string representing the name of the generated image
    '''
    try:
        # Determine if the snippet is in mermaid format
        int(snippet.strip()[0])
        msg = f"{Fore.YELLOW}The snippet for '{Fore.CYAN + file_name + Fore.YELLOW}' " \
              f"does not appear to be in mermaid format.{Fore.RESET}"
        print(msg)
        return f"INVALID_MERMAID_SNIPPET_{os.path.basename(file_name)}"
    except ValueError:
        # This is most likely a mermaid snippet so we need to continue processing
        pass
    out_dir = get_out_dir(topic, out_dir)
    temp_fname = "temp.md"

    if not os.path.exists(out_dir):
        print("{0}: Creating {1} because it doesn't exist.".format(
            Fore.GREEN + os.path.basename(__file__) + Fore.RESET,
            Fore.CYAN + out_dir + Fore.RESET
        ))
        os.makedirs(out_dir)

    # write the markdown only snippet to a file so it can be converted to an image
    with open(temp_fname, "w") as temp_md:
        temp_md.write(f"```mermaid\n{snippet}```")
    # run the markdown_mermaid_to_images program to create an image of the mermaid diagram
    subprocess.call(["markdown_mermaid_to_images", "-m", temp_fname, "-o", out_dir])
    # the conversion creates a markdown file in the output directory based on the original containing the file name
    # of the image. open the markdown file to get the image filename
    with open(f"{out_dir}/{temp_fname}") as temp_md:
        # strip away the markdown wrapping the file name by splitting the header off
        # and then splitting the closing parenthesis off
        auto_gen_fname = temp_md.read().split("![Image](")[1].split(")")[0]
    # Now that we know the auto generated file name, we can rename it to something more meaningful
    img_fname = qid + os.path.splitext(auto_gen_fname)[-1]
    os.rename(os.path.join(out_dir, auto_gen_fname), os.path.join(out_dir, img_fname))
    if os.path.exists(os.path.join(out_dir, temp_fname)):
        # Remove the temp file since it's no longer needed
        os.remove(os.path.join(out_dir, temp_fname))

    return img_fname


def snippet_image_generator(in_json_data: dict, in_json_file: str, out_dir: str = base_image_path,
                            font_file: str = font_file, font_size: int = 25, vrt_line_space: int = 6, tmargin: int = 20,
                            bmargin: int = 20,lmargin: int = 20, rmargin: int = 20, tl_point: tuple = (20, 20),
                            background_color: tuple = (0, 0, 0), text_color: tuple = (255, 255, 255),
                            height_correction: int = 5) -> int or str:
    '''
    Purpose: This function takes in a string representing 1) the path to a file, expected to be a question JSON file,
    and 2) the path to the directory where the new image should be saved. This function generates a PNG image based on
    the JSON file's snippet text. If the snippet is null then no image is generated. Upon successful operation, the
    new <uid>.png file is saved to the requested directory.
    :param in_json_data: This is the dictionary object of the question
    :param in_json_file: This is the path to a json file containing the question's properties
    :param out_dir: This is the path the resulting image should be saved
    :param font_file: This is the path to the font file used to process the image
    :param font_size: This is the font size to use in the output image; the default is 25
    :param vrt_line_space: The number of pixels between each line; the default is 6
    :param tmargin: The number of pixels in the top margin; the default is 20
    :param bmargin: The number of pixels in the bottom margin; the default is 20
    :param lmargin: The number of pixels in the left margin; the default is 20
    :param rmargin: The number of pixels in the right margin; the default is 20
    :param tl_point: The top left starting position in the generated image, expressed as an x,y tuple
                     The default is (20, 20)
    :param background_color: The RGB value, as a tuple, representing the color of the image's background
                             The default is black
    :param text_color: The RGB value, as a tuple, representing the color of the image's text; the default is white
    :param height_correction: An amount that's reduced from the line height, in pixels, to eliminate some of the extra
                              bottom margin that gets generated; default is 5
    :return: 6 = invalid integer; 5 = invalid tuple length; 4 = invalid output directory; 3 = invalid Input file;
             2 = Bad JSON Format; 1 = No snippet available; snippet = successful operation
    '''
    if not os.path.exists(in_json_file):
        # In case the input json file path is invalid
        print("{0} {1}: Unable to locate {2}.".format(
            Fore.RED + os.path.basename(__file__) + Fore.RESET,
            Fore.RED + error_str + Fore.RESET,
            Fore.RED + in_json_file + Fore.RESET
        ))
        return 3

    # If the font_file passed in does not exist or can not be found, use the defualt font file
    if not os.path.exists(font_file):
        # Find the defualt font file path
        font_file = resolve_fontfile_path()

    if not os.path.exists(font_file):
        # In case the font_file path is invalid
        print("{0} {1}: Unable to locate {2}.".format(
            Fore.RED + os.path.basename(__file__) + Fore.RESET,
            Fore.RED + error_str + Fore.RESET,
            Fore.RED + font_file + Fore.RESET
        ))
        return 3

    if len(tl_point) != 2:
        # In case the input tl_point has anything other than 2 items
        print("{0} {1}: tl_point expects a tuple of 2 items not {2}".format(
            Fore.RED + os.path.basename(__file__) + Fore.RESET,
            Fore.RED + error_str + Fore.RESET,
            Fore.RED + str(len(tl_point)) + Fore.RESET
        ))
        return 5

    for point in tl_point:
        if point < 0:
            # In case one or more tuple values are less than zero
            print("{0} {1}: tl_point value must be greater than zero".format(
                Fore.RED + os.path.basename(__file__) + Fore.RESET,
                Fore.RED + error_str + Fore.RESET
            ))
            return 6

    if len(background_color) != 3:
        # In case the input background_color has anything other than 3 items
        print("{0} {1}: background_color expects a tuple of 3 items not {2}".format(
            Fore.RED + os.path.basename(__file__) + Fore.RESET,
            Fore.RED + error_str + Fore.RESET,
            Fore.RED + str(len(background_color)) + Fore.RESET
        ))
        return 5

    for point in background_color:
        if point < 0:
            # In case one or more tuple values are less than zero
            print("{0} {1}: background_color value must be greater than zero".format(
                Fore.RED + os.path.basename(__file__) + Fore.RESET,
                Fore.RED + error_str + Fore.RESET
            ))
            return 6

    if len(text_color) != 3:
        # In case the input text_color has anything other than 3 items
        print("{0} {1}: text_color expects a tuple of 3 items not {2}".format(
            Fore.RED + os.path.basename(__file__) + Fore.RESET,
            Fore.RED + error_str + Fore.RESET,
            Fore.RED + str(len(text_color)) + Fore.RESET
        ))
        return 5

    for point in text_color:
        if point < 0:
            # In case one or more tuple values are less than zero
            print("{0} {1}: text_color value must be greater than zero".format(
                Fore.RED + os.path.basename(__file__) + Fore.RESET,
                Fore.RED + error_str + Fore.RESET
            ))
            return 6

    if vrt_line_space < 0:
        # In case the input integer is less than zero
        print("{0} {1}: vrt_line_space must be greater than zero.".format(
            Fore.RED + os.path.basename(__file__) + Fore.RESET,
            Fore.RED + error_str + Fore.RESET
        ))
        return 6

    if tmargin < 0 or bmargin < 0 or lmargin < 0 or rmargin < 0:
        # In case the input integer is less than zero
        print("{0} {1}: margin must be greater than zero.".format(
            Fore.RED + os.path.basename(__file__) + Fore.RESET,
            Fore.RED + error_str + Fore.RESET
        ))
        return 6

    try:
        # Open the json to extract code snippet using the "snippet" key
        out_dir = get_out_dir(in_json_data[topic_key], out_dir)
        # Make sure the output directory exists
        if not os.path.exists(out_dir):
            print("{0}: Creating {1} because it doesn't exist.".format(
                Fore.GREEN + os.path.basename(__file__) + Fore.RESET,
                Fore.CYAN + out_dir + Fore.RESET
            ))
            os.makedirs(out_dir)

        if os.path.exists(os.path.join(out_dir, f"{in_json_data[id_key]}.{image_type}")):
            # If the image was created on a previous iteration then don't create it again
            # Since this is run via CI/CD then the files won't exist unless the same question is generated twice
            # The exam generator is an example of when a question could be generated multiple times per pipeline run
            return os.path.join(out_dir, f"{in_json_data[id_key]}.{image_type}")

        # Get the code snippet via dictionary key
        if snippet_key in in_json_data:
            code_snippet = in_json_data[snippet_key]
            if code_snippet is None or code_snippet == "":
                print("{0}: No snippet to generate for {1}.".format(
                    Fore.GREEN + os.path.basename(__file__) + Fore.RESET,
                    Fore.CYAN + in_json_file + Fore.RESET
                ))
                return 1  # Necessary to prevent creation of an image with no text

            if topic_key not in in_json_data:
                print("{0} {1}: Input question missing 'topic' key.".format(
                    Fore.YELLOW + os.path.basename(__file__) + Fore.RESET,
                    Fore.YELLOW + warning_str + Fore.RESET
                ))
                # if the key does not exist then create it and set it to no topic
                in_json_data[topic_key] = ""

            if in_json_data[topic_key] is None:
                # In case the topic is set to None
                in_json_data[topic_key] = ""

            # Count the length of the longest line
            line_length = 0
            longest_line_index = 0
            index = 0

            for line in code_snippet.split("\n"):
                if len(line) > line_length:
                    line_length = len(line)
                    longest_line_index = index
                index += 1

            # Count the lines
            line_count = len(code_snippet.split("\n"))

            # Set font to the file. Set the font size to scale with number of lines needed
            font = ImageFont.truetype(font_file, font_size)
            # Get the pixel size of the longest line; x will be length and y will be height
            longest_line_size = font.getsize(code_snippet.split("\n")[longest_line_index])
            # Calculate the total number of pixels in the image's width
            # Must consider total length (in pixels) of the longest line and left/right margins
            image_width = longest_line_size[0] + lmargin + rmargin
            # Calculate the total number of pixels in the image's length. Must consider the height of the total
            # number of lines (in pixels), space between lines, and top/bottom margins
            if line_count > 4:
                # This is to reduce some of the extra bottom margin; the amount reduced was determined by trial/err
                line_height = longest_line_size[1] - height_correction
            else:
                # Snippets of 4 or less have acceptable bottom margins; one case has no bottom margin without this
                line_height = longest_line_size[1]
            image_length = line_height * line_count + (line_count - 1) * vrt_line_space + tmargin + bmargin

            # Make the background to write on
            background = Image.new('RGB', (image_width, image_length), background_color)

            # Write the code snippets on the black background
            text = ImageDraw.Draw(background)
            text.multiline_text(tl_point, code_snippet, font=font, fill=text_color, spacing=vrt_line_space)

            # Save the file using the _id
            snippet = "{0}.{1}".format(in_json_data[id_key], image_type)

            if not out_dir.endswith('/') and not out_dir.endswith("\\"):
                # In case the output directory is missing the trailing slash or backslash
                out_dir += '/'  # This works regardless of slash or backslash in path
            background.save(out_dir + snippet, image_type)

        else:
            # In case the file doesn't contain what we expect
            print("{0} {1}: Unable to process {2}; potentially bad JSON format.".format(
                Fore.RED + os.path.basename(__file__) + Fore.RESET,
                Fore.RED + error_str + Fore.RESET,
                in_json_file
            ))
            return 2

    except Exception as e:
        print("{0} {1}: Unable to generate image for {2}: {3}".format(
            Fore.RED + os.path.basename(__file__) + Fore.RESET,
            Fore.RED + error_str + Fore.RESET,
            Fore.RED + in_json_file + Fore.RESET,
            e
        ))
        return 4

    # Return the image name used during generation
    # Returning just the file name allows compatibility with the Google Forms Script during CSV file creation
    return snippet
