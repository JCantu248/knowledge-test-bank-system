// Globals used in the form
nameQuestionText = "Please provide your full name.";
rankQuestionText = "Please provide your rank.";
iqtQuestionText = "Did you go through IQT";
trainingQuestionText = "Prior to taking this exam, did you complete training for ####?";
correctAnswerTitle = "Correct Answer";
questNameTitle = "Question Name";
questUidTitle = "Question UID";

// ======== CREATES FEEDBACK FORM AND SENDS LINK TO USER AFTER THEY SUBMIT EXAM =========================
function onFormSubmit(e){
  
  // ==================== GET INFO FROM TRIGGER EVENT =========
  var user = {time: e.namedValues['Timestamp'][0], email: e.namedValues['Email Address'][0]};
  Logger.log("---- user == " + user.email + "-----");
  
  var fileName = getSourceFileFromTrigger(e.triggerUid) + "_feedback";
  Logger.log("---- fileName == " + fileName + "-----");
  // ==========================================================
  
  
  // GET EXISTING FOLDER, OR CREATE A NEW ONE TO HOLD FEEDBACK
  var feedbackFolder = DriveApp.getFoldersByName("KnowledgeFeedback");
  var folder;
  while(feedbackFolder.hasNext()){
    folder = feedbackFolder.next();
    break;
  }
  if (!folder) {
    folder = DriveApp.createFolder("KnowledgeFeedback");
  }
  Logger.log("---- folder == " + folder);
  
 
  // ======================= SET TEXT AND QUESTIONS FOR FEEDBACK FORM ======================
  var intro = 'As a recent examinee, the Stan/Eval Section would appreciate your feedback about your exam experience. ' +
  'The details you provide will be used to enhance the exam itself as well as our processes and procedures. ' +  
  'We look forward to your candid thoughts. ' +  
  'Please contact the Stan/Eval Office with any questions or concerns. ' +
  'Thank you for your time! ' +
  'The Stan/Eval Office90IOS.DOT.INBOX@us.af.milCOM:  (210) 977-2561  DSN 969-2561 ';
    
  //Write feedback questions here and just plug in the variable
  var q1Title = "Do you feel you were adequately prepared for this exam?"
  var q1FollowUp = "If not, describe what areas you were not trained in or where you were not mentored properly:"
  var q2Title = "Did you have an adequate amount of time to take this exam?"
  var q2FollowUp = "How much more time do you belive should be given?"
  var q3Title = "Was the exam environment sufficient?"
  var q3FollowUp = "If not, describe what areas you were not trained in or where you were not mentored properly:"
  var q4Title = "Were the exam instructions clear?"
  var q4FollowUp = "If not, describe what areas you were not trained in or where you were not mentored properly:"
  var q5Title = "If there were any other points you would like to share with the Stan/Eval Office, enter them below:"
  var qFinalTitle = "May we contact you about this feedback?"
  var qFinalFollowUp= "If so, please include your contact information in the fields below:"
  
  
  
  // variable to hold our feedaback form
  var feedbackForm;
  
  //If the form doesnt exist, create it and generate questions.
  if (checkForFile(fileName) === false){
    feedbackForm = FormApp
    .create(fileName)
    .setDescription(intro)
    .setAllowResponseEdits(false)
    .setLimitOneResponsePerUser(false)
    .setProgressBar(true)
    .setAcceptingResponses(true)
    .setConfirmationMessage("Thank you for the feedback!");
    
    //Ask exam type
    var examType = feedbackForm.addMultipleChoiceItem();
    examType.setTitle("Evaluation Type:")
    .setChoices([
      examType.createChoice('Basic Dev'),
      examType.createChoice('Senior-Linux'),
      examType.createChoice('Senior-Windows'),
      examType.createChoice('Basic PO')
    ])
    .showOtherOption(false);
    
    //Q1
    createFeedbackQ(feedbackForm, q1Title, q1FollowUp);
    //Q2
    createFeedbackQ(feedbackForm, q2Title, q2FollowUp);
    //Q3
    createFeedbackQ(feedbackForm, q3Title, q3FollowUp);
    //Q4  
    createFeedbackQ(feedbackForm, q4Title, q4FollowUp);
    //Q5
    var q5Explain = feedbackForm.addParagraphTextItem()
    .setTitle(q5Title)
    .setRequired(false);
    //QFinal
    createFeedbackQ(feedbackForm, qFinalTitle, qFinalFollowUp);
    //Move feedback form to the "KnowledgeFeedback" folder
    moveFiles(feedbackForm,folder.getId());
    Logger.log("---- did not find fileName ----");

  } else {
    // if we already have a feedback have a form, grab it
    var files = DriveApp.getFilesByName(fileName);
    while(files.hasNext()){
      var file = files.next();
    }  
    if (file) {
      Logger.log("---- found fileName == " + fileName);
      feedbackForm = FormApp.openById(file.getId());
      Logger.log("---- found feedbackForm == " + feedbackForm);
    }
  }
   
  // ------- creates feedback response sheet -----------------------
  var responseSheet = fileName + "_responses";
  var feedbackResponseSheet;
  
  // If the reponse sheet doesnt exist, create it
  if(checkForFile(responseSheet) === false){
    feedbackResponseSheet = SpreadsheetApp.create(responseSheet);
    if (feedbackForm) {
      feedbackForm.setDestination(FormApp.DestinationType.SPREADSHEET, feedbackResponseSheet.getId());
      Logger.log("---- set form destination ----");
      
      //Move feedback form to the "KnowledgeFeedback" folder
      moveFiles(feedbackResponseSheet,folder.getId());
    }
  } 
  //If the response sheet exists, open 
  if(checkForFile(responseSheet) === true) {
    var files = DriveApp.getFilesByName(responseSheet)
    while(files.hasNext()){
      feedbackResponseSheet = files.next();
    } 
    feedbackResponseSheet = SpreadsheetApp.openById(feedbackResponseSheet.getId())
  }
  
  
  Logger.log("---- sent email to " + user.email + ". Link is " + feedbackForm.getPublishedUrl());
  sendEmail(user.email, user.time, feedbackForm.getPublishedUrl());   
}


// ======= FINDS SOURCE FILENAME WHERE TRIGGER OCCURRED =================
function getSourceFileFromTrigger(triggerUid) {
  var fileName = "";
  var allTriggers = ScriptApp.getProjectTriggers();
  var allTriggersLen = allTriggers.length;
  
  // iterate through triggers and match id's
  for (var i = 0; i < allTriggersLen; i++){
      var trigger = allTriggers[i];
      trigger.getTriggerSourceId();
      if (triggerUid == trigger.getUniqueId()){
        fileName = DriveApp.getFileById(trigger.getTriggerSourceId()).getName();
        break;
      }
  }
  return fileName;
}


// =========== CHECKS IF FILE EXISTS ================
function checkForFile(filename){
  return DriveApp.getFilesByName(filename).hasNext();
}


// =========== MOVES FILE TO TARGET DIRECTORY ==============
function moveFiles(item, targetFolderId) {
  var id = item.getId();
  var file = DriveApp.getFileById(id);
  file.getParents().next().removeFile(file);
  DriveApp.getFolderById(targetFolderId).addFile(file);
}


// ========== SENDS EMAIL TO ADDRESS ========================
function sendEmail(address, date, url){
  var response = UrlFetchApp.fetch(url);
  var htmlBody = HtmlService.createHtmlOutput(response).getContent();
  MailApp.sendEmail(address,
                    "Please provide us with feedback from your recent exam on " + date,
                    'This message requires HTML support to view.',
                    ("Please provide us with feedback from your recent exam by visiting this link " + url));
  Logger.log("Email sent to " + address);
}


function createFeedbackQ(feedbackForm, title, titleFollowUp){
  var q = feedbackForm.addMultipleChoiceItem();
  q.setTitle(title)
  .setChoices([
          q.createChoice('Yes'),
          q.createChoice('No'),
  ])
  .showOtherOption(false)
  .setRequired(true);
  var qExplain = feedbackForm.addParagraphTextItem()
  .setTitle(titleFollowUp)
  .setRequired(false);
}


    
    
// ============== POPULATE DROPDOWNS ON PAGE LOAD =================== 
function onOpen() {
  var sheet = SpreadsheetApp.getActiveSpreadsheet().getActiveSheet();
//  var cellStart = 16; // hardcoded for now....
//  var folderName = "KnowledgeMQL";
//  
//  var fileList = getFileList(folderName);
//  var fileListLen = fileList.length;
//  if (fileListLen > 0) {
//    for (var i = 0; i < fileListLen; i++) {
//      var cell = sheet.getRange("A" + (cellStart + i));
//      cell.setValue(fileList[i]);
//    }
//  }
}


// ============= GETS AN ARRAY OF FILES IN A FOLDER =================
function getFileList(folderName) {
  // get the question folder by name
  var folder = DriveApp.getFoldersByName(folderName).next();
  
  // grab filenames from folder
  var fileList = [];
  var files = folder.getFiles();
  while (files.hasNext()) {
    var currentFile = files.next()
    fileList.push(currentFile.getName());
  }
  return fileList;
}


// ============= GETS A SPECIFIC FILE IN A FOLDER =================
function getFile(folderName, mqfName) {
  // get the question folder by name
  var folder = DriveApp.getFoldersByName(folderName).next();
  
  // grab filenames from folder
  var files = folder.getFiles();
  var currentFile = null;
  while (files.hasNext()) {
    currentFile = files.next();
    if (currentFile.getName() === mqfName) {
      break;
    }
  }
  return currentFile;
}


// ============= GRABS AVAILABLE CHOICES FOR QUESTION ====================
function getChoices(options, answer, item) {
  var choices = [];
  var keysArray = Object.keys(options);
  var keysLength = keysArray.length;
  for (var j = 0; j < keysLength; j++) {
    var key = keysArray[j];
    
    // need to account for '0' and 'false' as potential answers
    if (options[key] !== '') {
      // create choice along with boolean for correct value; add to choices
      var choice = item.createChoice(options[key], key == answer.toUpperCase());
      choices.push(choice);
    }
  }
  return choices;
}


// ============ GETS IMG FROM FILENAME ===================================
function getImg(image) {
  var imgs = DriveApp.getFilesByName(image);
  var img;
  while (imgs.hasNext()) {
    var img = imgs.next();
  }
  // return img object if found, or an empty object if not found
  return img;
}


// ======== Addes each row of a 2D array to another 2D array ========
function append2D(destArray, twoDArray) {
  for each (var row in twoDArray) {
    destArray.push(row);
  }
  return destArray
}


function getExamQuestions(folderName, fileName, colLabelRowNumber) {
  var questions = [];
  // find file by fileName but only search the selected test bank folder
  var file = getFile(folderName, fileName);
  
  // ensure we found a file
  if (file) {
    // open spreadsheet
    var spreadSheet = SpreadsheetApp.open(file);
    var rows = spreadSheet.getActiveSheet().getDataRange().getValues();
    
    questions = rows.filter(function(row, index){
      // Add all questions to the list in the order received being sure to skip the column label row
      if (index !== colLabelRowNumber - 1) {
        return row;
      }
    });
  }
  return questions;
}


// ============= Builds list of questions by subject ======================
function getExamQuestionsBySubject(folderName, fileName, subjects, workRole, colLabelRowNumber) {
  var questions = [];
  // check for 0 or no input
  if (Object.keys(subjects).length > 0) {
    
    // find file by fileName but only search the selected test bank folder
    var file = getFile(folderName, fileName);
    
    // ensure we found a file
    if (file) {
      // open spreadsheet
      var spreadSheet = SpreadsheetApp.open(file);
      var rows = spreadSheet.getActiveSheet().getDataRange().getValues();
      var colLabels = rows[colLabelRowNumber - 1];
      var subjIndex = 0; // Stores the index number with the subject data
      // Determin the subject index
      colLabels.forEach(function(label, index){
        if (label === "Subject:"){
          subjIndex = index;
        }
      });
      var reWorkRole = new RegExp("^" + workRole); // Search pattern to find the work role name at the beginning
      for (var subjKey in subjects) {
        // Process each subject
        if (reWorkRole.test(subjKey)){
          // In case this subject is for the requested work role
          if (workRole !== subjKey) {
            // In case this work role has multiple subjects, reduce the string to just the subject name
            var subj = subjKey.replace(reWorkRole, "").replace(/^_/, "");
          } else {
            // The Work Role name is the same as the subject
            subj = subjKey;
          }
          var subjQuestions = rows.filter(function(row, index){
            // Add all questions that are identified as the subject to the list
            if (row[subjIndex] === subj){
              return row;
            }
          });
          // Populate the exam questions list with the requested number of random questions for this subject
          append2D(questions, getRandomQuestions(subjQuestions, subjects[subjKey]));
        }
      }
    }
  }
  return questions;
}


// ============= GETS RANDOM QUESTIONS ====================================
function getRandomQuestions(questionList, number) {
  var questions = [];
  // check for 0 or no input
  if (number > 0 && number != "" && number <= questionList.length) {
    for (var i = 0; i < number; i++) {
      // generate random number, from 0 - questionNum (not inclusive)
      var randomNum = Math.floor(Math.random() * questionList.length);
      
      // add that question data to questions
      questions.push(questionList[randomNum]);
      
      // remove the randomly picked question so it can't be picked again
      questionList.splice(randomNum, 1);
    }
  }
  return questions;
}


// === Discovers the Index Number of Column Headers ===
function getQuestionIndexData(filename, colLabelRowNumber) {
  var questTypeLabel = "Question Type:";
  var questionNameLabel = "Question Name:";
  var questionLabel = "Question:";
  var answerLabel = "Answer:";
  var explainLabel = "Explanation:";
  var pointsLabel = "Points:";
  var subjectLabel = "Subject:";
  var trainingRefLabel = "Training Ref:";
  var imageNameLabel = "Image Name:";
  var workroleLabel = "Work Role:";
  var uidLabel = "UID:";

  if (colLabelRowNumber < 1) {
    // In case the input colLabelRowNumber is invalid
    Logger.log("The column label row number must be greater than 0");
    return null;
  }
  var questionOptionsInfo = {
    QuestionTypeIndex: 0,
    QuestionNameIndex: 0,
    QuestionIndex: 0,
    MaxOptions: 0,
    StartIndex: 0,
    AnswerIndex: 0,
    ExplanationIndex: 0,
    PointsIndex: 0,
    SubjectIndex: 0,
    TrainingRefIndex: 0,
    QuestionImageIndex: 0,
    WorkroleIndex: 0,
    UidIndex: 0
  }; // Stores the question multiple choice option data
  var colLabels = []; // Stores the column labels
  var previous = false;
  var isOptionChar = false;
  // find file by filename
  var files = DriveApp.getFilesByName(filename);
  var file;
  while (files.hasNext()) {
    file = files.next();
  }
    
  // ensure we found a file
  if (file) {
    // open spreadsheet
    var spreadSheet = SpreadsheetApp.open(file);
    var rows = spreadSheet.getActiveSheet().getDataRange().getValues();
    colLabels = rows[colLabelRowNumber - 1];
    // Determin the multiple choice option information
    colLabels.forEach(function(label, index){
      if (label.toLowerCase() === questTypeLabel.toLowerCase()){
        questionOptionsInfo.QuestionTypeIndex = index;
      }
      if (label.toLowerCase() === questionLabel.toLowerCase()){
        questionOptionsInfo.QuestionIndex = index;
      }
      if (label.length === 1 && label.match(/[A-Z]/i)){
        // In case this label is a multiple choice option label
        questionOptionsInfo.MaxOptions++;
        isOptionChar = true;
      } else {
        isOptionChar = false;
      }
      if (previous == false && isOptionChar == true){
        // In case this is the start of the multiple choice options
        questionOptionsInfo.StartIndex = index;
      }
      if (label.toLowerCase() === answerLabel.toLowerCase()){
        questionOptionsInfo.AnswerIndex = index;
      }
      if (label.toLowerCase() === explainLabel.toLowerCase()){
        questionOptionsInfo.ExplanationIndex = index;
      }
      if (label.toLowerCase() === pointsLabel.toLowerCase()){
        questionOptionsInfo.PointsIndex = index;
      }
      if (label.toLowerCase() === subjectLabel.toLowerCase()){
        questionOptionsInfo.SubjectIndex = index;
      }
      if (label.toLowerCase() === trainingRefLabel.toLowerCase()){
        questionOptionsInfo.TrainingRefIndex = index;
      }
      if (label.toLowerCase() === imageNameLabel.toLowerCase()){
        questionOptionsInfo.QuestionImageIndex = index;
      }
      if (label.toLowerCase() === workroleLabel.toLowerCase()){
        questionOptionsInfo.WorkroleIndex = index;
      }
      if (label.toLowerCase() === uidLabel.toLowerCase()){
        questionOptionsInfo.UidIndex = index;
      }
      if (label.toLowerCase() === questionNameLabel.toLowerCase()){
        questionOptionsInfo.QuestionNameIndex = index;
      }
      previous = isOptionChar;
    });
  }
  return questionOptionsInfo;
}


function addIDoNotKnowOption(options, lastOptionLetter) {
  // Purpose: Add an "I do not know" option at the end of all choices
  // param: options: A key => value object of the current multiple choice options
  // param: lastOptionLetter: An ASCII code representing the next available letter in the multiple choice options
  optionLetter = String.fromCharCode(lastOptionLetter);
  options[optionLetter] = optionLetter + ") " + "I do not know"
}


// ================= ADDS CHOICE QUESTIONS TO FORM ===============================
function addChoiceQuestionsToForm(form, questions, questionIndexData, useAllQuestions, randQuestions) {
  var prevSubject = "";
  // For easy access to choices/answer/explanation(s)
  var optionMap = {
    "A": 0,
    "B": 1,
    "C": 2,
    "D": 3,
    "E": 4,
    "F": 5,
    "G": 6,
    "H": 7,
    "I": 8
  };
  if (useAllQuestions === undefined) {
    // In case useAllQuestions is not specified
    useAllQuestions = false;
  }
  for each (var question in questions) {
    // set our variables for each question
    var questionType = question[questionIndexData.QuestionTypeIndex];
    var questionText = questCounter++ + ')  ' + question[questionIndexData.QuestionIndex];
    var options = {};  // Stores all options for this multiple choice question
    var option = 'A'.charCodeAt(0);  // The current option
    for (var i = questionIndexData.StartIndex; i < (questionIndexData.MaxOptions + questionIndexData.StartIndex); i++){
      // Add each multiple choice option to the options dictionary
      if (String(question[i])){
        // In case this option has data
        var optionLetter = String.fromCharCode(option);
        options[optionLetter] = optionLetter + ") " + question[i];
      }
      else {
        // In case there are no more options for this question
        break;
      }
      ++option;
    }
    addIDoNotKnowOption(options, option)  // Add the I do not know option
    var answer = question[questionIndexData.AnswerIndex];
    var explanations = question[questionIndexData.ExplanationIndex].split("\n");  // Convert to an array
    var pointVal = question[questionIndexData.PointsIndex];
    
    // below is most likely to change
    var subject = question[questionIndexData.SubjectIndex];
    var trainingRef = question[questionIndexData.TrainingRefIndex];
    var image = question[questionIndexData.QuestionImageIndex];
    var workrole = question[questionIndexData.WorkroleIndex];
    
    if (questionType === 'CHOICE') {
      // add a section header if it's the first question
      if (prevSubject !== subject && !useAllQuestions && randQuestions === 0) {
        var header = form.addSectionHeaderItem();
        header.setTitle(subject);
      }
      // fetch and include image above question if one is specified
      if (image) {
        var img = getImg(image);
        // make sure img returned isn't empty/NULL
        if (img) {
          img = img.getBlob();
          form.addImageItem()
          .setImage(img)
          .setAlignment(FormApp.Alignment.CENTER);
        }
      }
      // add the question to the form
      var item = form.addMultipleChoiceItem();
      item.setTitle(questionText)
      .setChoices(getChoices(options, answer, item));
      // Setup the feedback given when the response is correct or incorrect
      var feedbackCorrect = FormApp.createFeedback();
      var feedbackIncorrect = FormApp.createFeedback();
      if (explanations.length > 1){
        // In case there is feedback for each option, setup the correct response feedback
        var opts = Object.keys(optionMap);  // A list of option letters starting with A
        for (var expIndex in explanations){
          // Insert each option letter in front of each explanation, storing it back in the array
          explanations[expIndex] = opts[expIndex] + ") " + explanations[expIndex].toString();
        }
        feedbackCorrect.setText(explanations[optionMap[answer]]);  // Setup correct feedback message
      } else {
        // In case there's only one explanation for all options, setup to display for correct responses
        feedbackCorrect.setText(explanations.toString());
      }
      item.setFeedbackForCorrect(feedbackCorrect.build());  // Add correct feedback to the item's correct feedback attribute
      feedbackIncorrect.setText(explanations.join("\n"));  // Setup incorrect feedback message
      item.setFeedbackForIncorrect(feedbackIncorrect.build());  // Add incorrect feedback to the item's incorrect feedback attribute
      
      // can cause errors if CHOICE doesn't have pointVal, so evaluate it here
      if(pointVal) {
        item.setPoints(pointVal);
      }
      prevSubject = subject;
    }
  }
  Utilities.sleep(50);  // Helps avoid form/resource access errors
}


// ======== ADDS GENERAL Q'S ASKED ON EVERY EXAM ===============
function addGeneralQuestionsToForm(form, workrole) {
  // name, rank and training/not
  form.addTextItem()
  .setTitle(nameQuestionText)
  .setRequired(true);
  
//  form.addTextItem()
//  .setTitle(rankQuestionText)
//  .setRequired(true);
  
  form.addMultipleChoiceItem()
  .setTitle(trainingQuestionText.replace("####", workrole))
  .setChoiceValues(["Yes", "No"])
  .setRequired(true);
}

// ====== Adds a response to the form's questions. This allows us to see things like correct answers and question UIDs/names in the response sheet ======
function addResponseToForm(form, questionList, questionIndexData, entryTitle, workrole) {
  var questionIndex = 0;  // To grab questions from questionList through iteration
  form.setCollectEmail(false);  // Necessary to add a response via script as there's no way to add an e-mail
  var formItems = form.getItems();  // Stores all questions on the form
  var formResponse = form.createResponse();
  for each (var item in formItems){
    // Set responses for each question on the form
    var itemsType = item.getType().toString();  // The item's type
    if (itemsType == "MULTIPLE_CHOICE"){
      var mcItem = item.asMultipleChoiceItem();
      var mcQuestion = mcItem.getTitle().replace(/^[0-9]+\)\s+/g, "");  // Strips off the leading #) before the question
      if (trainingQuestionText.replace("####", workrole) == mcQuestion){
        formResponse.withItemResponse(mcItem.createResponse("No"));  // Set training question response
      } else if (correctAnswerTitle == entryTitle){
        var mcChoices = mcItem.getChoices();
        for each (var choice in mcChoices){
          // Search choices for the correct answer
          if (choice.isCorrectAnswer()){
            formResponse.withItemResponse(mcItem.createResponse(choice.getValue()));  // Set correct answer response
            break;
          }
        }
      } else if (questUidTitle == entryTitle){
        mcItem.showOtherOption(true);  // Necessary to add information that's not a valid choice
        var qUid = questionList[questionIndex++][questionIndexData.UidIndex];
        formResponse.withItemResponse(mcItem.createResponse(qUid));  // Add the question's UID as a response
        mcItem.showOtherOption(false);  // Reset so examinee's don't have an other option
      } else if (questNameTitle == entryTitle){
        mcItem.showOtherOption(true);  // Necessary to add information that's not a valid choice
        var qName = questionList[questionIndex++][questionIndexData.QuestionNameIndex];
        formResponse.withItemResponse(mcItem.createResponse(qName));  // Add the question's name as a response
        mcItem.showOtherOption(false);  // Reset so examinee's don't have an other option
      }
    }
    if (itemsType == "TEXT"){
      var textItem = item.asTextItem();
      if (/full name/.test(textItem.getTitle().toLowerCase())){
        formResponse.withItemResponse(textItem.createResponse(entryTitle));  // Add the purpose of response as the full name
      }
    }
  }
  formResponse.submit();  // Send the responses to the form
  form.setCollectEmail(true);  // Reset so examinee's have to enter their e-mail address
  Utilities.sleep(50);  // Helps avoid form access errors
}

// ======================================================================================================
// This function creates a section break in the input form with the given title and description
// ======================================================================================================
function addSectionBreak(form, title, description){
  if (title === undefined){
    // In case the title wasn't provided as an input parameter
    title = "New Section"
  }
  if (description === undefined){
    // In case the description wasn't provided as an input parameter
    description = ""
  }
  form.addPageBreakItem().setTitle(title).setHelpText(description);
}


// ======================================================================================================
// This functions creates a form from the active sheet/tab in the currently active spreadsheet
// ======================================================================================================
function formCreator() {
  //Delete triggers before adding new one at the end of the script
  var allTriggers = ScriptApp.getProjectTriggers();
  for(var i = 0; i < allTriggers.length; i++){
      ScriptApp.deleteTrigger(allTriggers[i]);
  }
  
  // =============== OPEN ACTIVE SPREADSHEET ================================
  var ss = SpreadsheetApp.getActiveSpreadsheet(); // grabs the active sheet/tab from the active spreadsheet
  var sheet = ss.getActiveSheet()
  var range = sheet.getDataRange();
  var numberRows = range.getNumRows();
  var data = range.getValues();
  var folderName = "TestBank"; // Default Value
  var mqfName = "basic-dev_MQF"; // Default Value
  var colLabelRowNumber = 0;
  var randQuestions = 0; // Default Value
  var workRole = "Basic Dev"; // Default Value
  var title = "";
  var testVersion = "Test Version: ";
  var useAllQuestions = false; // Default Value
  
  // ============= ITERATE THROUGH ROWS IN SHEET =============================
  for(var i = 0; i < numberRows; i++){
    // data[i][j] --> i = row, j = column
    
    // set our variables for each row
    var columnTitle = data[i][0];  //start @ row 1, column A
    
    if (columnTitle == 'Exam Title') {
      // we found top level elements.....
      title = data[i][1]; // Set User Desired Value for `Exam Title`
      folderName = data[i + 1][1]; // Set User Desired Value for `MQF Folder Name`
      mqfName = data[i + 2][1]; // Set User Desired Value for `MQF Name`
      colLabelRowNumber = data[i + 3][1]; // Set User Desired Value for `Column Label Row Number`
      randQuestions = data[i + 4][1]; // Set User Desired Value for `For Random Question Test, Enter Total Questions`
      workRole = data[i + 5][1]; // Set User Desired Value for `Work Role To Evaluate`
      useAllQuestions = data[i + 7][1]; // Set User Desired Value for `Provided MQF represents the entire exam?`
      var form = FormApp
      .create(title)  // grab and set the title of the form
      .setDescription(data[i + 6][1] + "\n" + testVersion + title) // Set according to User Desired Value for `Description/Instructions`
      .setAllowResponseEdits(data[i + 8][1]) // cannot edit answers after submitting
      .setLimitOneResponsePerUser(data[i + 9][1])  // only allow a single reponse from each user
      .setCollectEmail(data[i + 10][1])  // collect email from each submission
      .setPublishingSummary(data[i + 11][1])
      .setProgressBar(true)
      .setShowLinkToRespondAgain(false)
      .setIsQuiz(true);
      // time limit?
      // any other important data
      
      //=============== CREATE THE RESPONSE SPREASHEET ====================
      var responseSheet = SpreadsheetApp.create(title + '_responses');
      form.setDestination(FormApp.DestinationType.SPREADSHEET, responseSheet.getId());
      
      // ============== ADD GENERAL QUESTIONS TO FORM ======================
      addGeneralQuestionsToForm(form, workRole);
      addSectionBreak(form, "Exam");
      
    }
    else if (columnTitle == 'Subject') {
      // =============== ADD QUESTIONS TO FORM BASED ON USER INPUT =========
      // get list of files so we know how many subjects to iterate through
      questCounter = 1 // Global stores next available question number in exam
      var subjectQuestions = [];
//      var fileList = getFileList(folderName);
      if (randQuestions > 0) {
        subjectQuestions = getRandomQuestions(getExamQuestions(folderName, mqfName, colLabelRowNumber), randQuestions);
      } else if (useAllQuestions === false) {
        var numSubjects = 0;
        var subjects = {};
        var subject = data[i + 1][0];
        while (subject && (i + (++numSubjects)) < numberRows) {
          subject = data[i + numSubjects][0];
          subjects[subject] = data[i + numSubjects][1];
        }
        subjectQuestions = getExamQuestionsBySubject(folderName, mqfName, subjects, workRole, colLabelRowNumber);
      } else if (useAllQuestions === true) {
        subjectQuestions = getExamQuestions(folderName, mqfName, colLabelRowNumber);
      }
      var questionIndexData = getQuestionIndexData(mqfName, colLabelRowNumber);
      addChoiceQuestionsToForm(form, subjectQuestions, questionIndexData, useAllQuestions, randQuestions);
      addSectionBreak(form, "Review & Submit", "Please review your answers (by clicking the back button below) " +
                      "to make sure you've answered every question to your satisfaction. Once you're happy with your answers, " +
                      "select submit to complete your exam and receive your grade.");
      break;
    }
  }
  // Add the correct answers as a response for access in the Google Sheet copy of responses
  addResponseToForm(form, subjectQuestions, questionIndexData, correctAnswerTitle, workRole);
  // Add question identifying information as a response in order to update the Test Bank with the results
  addResponseToForm(form, subjectQuestions, questionIndexData, questUidTitle, workRole);
  addResponseToForm(form, subjectQuestions, questionIndexData, questNameTitle, workRole);
  //Create trigger for onOpen
//  ScriptApp.newTrigger('onOpen').forSpreadsheet(ss).onOpen().create();
  
  //  //Create trigger for form submission -- DISABLED FOR NOW BC OF ISSUES WITH FILLING OUT THE FORM IN BROWSER
  //  ScriptApp.newTrigger('onFormSubmit').forSpreadsheet(responseSheet).onFormSubmit().create();
}
